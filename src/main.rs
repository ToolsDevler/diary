use std::env;
use std::fs;
use std::fs::OpenOptions;
use std::io::Write;
use std::path::Path;
use chrono::{Local, Datelike, Timelike};

fn main() {

    let mut args: Vec<String> = env::args().collect();

    //
    // Determine diary file:
    //
    let home_dir = match env::var("HOME") {
        Ok(val) => val,
        Err(_) => {
            eprintln!("Error: Failed to retrieve HOME directory");
            std::process::exit(1);
        }
    };


    let diary_dir = Path::new(&home_dir).join(".diary");
    if !diary_dir.exists() {
        match fs::create_dir(&diary_dir) {
            Ok(_) => (),
            Err(e) => {
                eprintln!("Error: Failed to create diary directory: {}", e);
                std::process::exit(1);
            }
        }
    }

    //
    // Calculate target date
    //
    let mut date_offset = if args.len() == 1 { 0 } else { -1 };

    //
    // Calculate offset if given
    //
    if args.len() > 1 && args[1] == "-d" {
        match args.get(2) {
            Some(offset) => match offset.parse::<i32>() {
                Ok(val) => date_offset = val,
                Err(_) => {
                    eprintln!("Error: Invalid offset argument");
                    std::process::exit(1);
                }
            },
            None => {
                eprintln!("Error: Missing offset argument");
                std::process::exit(1);
            }
        };
    }
    
    let mut date = Local::now();

    if date_offset != -1 {
        //
        // Calculate the target date if given
        //
        date = date.checked_sub_signed(chrono::Duration::days(date_offset.into())).unwrap();
    }

    //
    // View or append file.
    //

    let file_name = format!("{}{:02}{:02}", date.year(), date.month(), date.day());
    let diary_file = diary_dir.join(file_name);
    if !diary_file.exists() {
        if date_offset == -1 {
            match fs::File::create(&diary_file) {
                Ok(_) => (),
                Err(e) => {
                    eprintln!("Error: Failed to create diary file: {}", e);
                    std::process::exit(1);
                }
            }
        } else {
            std::process::exit(0);
        }
    }

    if date_offset != -1 {
        let diary_content = match fs::read_to_string(&diary_file) {
            Ok(val) => val,
            Err(e) => {
                eprintln!("Error: Failed to read diary file: {}", e);
                std::process::exit(1);
            }
        };
        println!("{}", diary_content);
    } else {
        args.remove(0);
        let mut file = OpenOptions::new()
            .write(true)
            .append(true)
            .open(&diary_file)
            .unwrap();
        let contents = format!("{}:{:02}: {}", date.hour(), date.minute(), args.join(" "));
        if let Err(e) = writeln!(file, "{}", contents) {
            eprintln!("Error for file '{}': {}", diary_file.to_str().unwrap(), e);
            std::process::exit(1);
        }
    }
}